const express = require('express')
const timelineItem = require('./timelineItem')
const cors = require('cors')

const routes = express.Router()

routes.use(cors())
routes.use('/api', timelineItem)

module.exports = routes
