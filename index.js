const express = require('express')
const routes = require('./routes/index')
const bodyParser = require('body-parser')

const app = express()

const port = process.env.PORT || 3001

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(routes)

app.listen(port, () => console.log(`server running on port ${port}`))
